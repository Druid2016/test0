
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import time

from xml.dom.minidom import *
from base import *
from page import *
from locators import *


class TestSBR():

    def setup(self):
        xml = parse("SBR.xml")
        url = xml.getElementsByTagName("address")[0].firstChild.nodeValue
        self.driver = webdriver.Chrome()
        self.driver.get(url)
        time.sleep(3)

    def teardown(self):
        self.driver.quit()


    def test_container_displayed(self):

        try:
            assert page.container_displayed().is_displayed()
        except:
            AssertionError('Widget container is not displayed on page')
        print 'Container is displayed'

    def test_button_toolbar(self):
        driver = self.driver
        buton = driver.find_element(*Sber_Widget_Locators.BUTTON_WIDGET_ICON_HIDER)
        buton.click()

        try:
            assert page.Title_Widget_Displayed().is_displayed()
        except:
            AssertionError('Title widget is not displayed on page')
        print 'Title widget is displayed'

        mini_button = driver.find_element(*Sber_Widget_Locators.MINIMIZE_BUTTTON)
        mini_button.click()



    def test_catalog_button(self):
        driver = self.driver
        button = driver.find_element(*Sber_Widget_Locators.BUTTON_WIDGET_ICON_HIDER)
        button.click()

        button_1 = driver.find_element(*Sber_Widget_Locators.BUTTON_CATALOG_W)
        button_1.click()

        try:
            assert page.Widget_Catalog_Displayed().is_displayed()
        except:
            AssertionError('Widget catalog is not displayed on page')
        print 'Widget catalog is displayed'