from selenium import webdriver
from selenium.webdriver.common.by import By


class Sber_Widget_Locators(object):
    WIDGET_CONTAINER = (By.CSS_SELECTOR, 'div.bp-container.sbt-springboard-container')
    WIDGET_ICON = (By.CLASS_NAME, 'widget-icons')
    WIDGET_TITLE = (By.CLASS_NAME, 'personalized-widget-title')
    WIDGET_CATALOG = (By.CLASS_NAME, 'sbrf-widget-catalog')

    BUTTON_WIDGET_ICON_HIDER = (By.CLASS_NAME, 'widget-icon-hider')
    BUTTON_CATALOG_W = (By.CLASS_NAME, 'catalog-w')
    MINIMIZE_BUTTTON = (By.CLASS_NAME, 'minimize-w')