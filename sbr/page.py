# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait

from base import Page
from locators import *


class Widget_Page(Page):

    def container_displayed(self):

        return self.find_element(*Sber_Widget_Locators.WIDGET_CONTAINER)

    def Widget_icons_Displayed(self):

        return self.find_element(*Sber_Widget_Locators.WIDGET_ICON)

    def Title_Widget_Displayed(self):

        return self.find_element(*Sber_Widget_Locators.WIDGET_TITLE)


    def Widget_Catalog_Displayed(self):

        return self.find_element(*Sber_Widget_Locators.WIDGET_CATALOG)