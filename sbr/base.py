# -*- coding: utf-8 -*-

import random
from selenium import webdriver
from selenium.webdriver.common.by import By



class Page(object):
    def __init__(self, driver, url):
        self.url = url
        self.driver = driver

    def get_url(self):
        return self.driver.current_url

    def open(self, url):
        url = self.url
        self.driver.get(url)

    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    def click(self, *locator):
        self.driver.find_element(*locator).click()