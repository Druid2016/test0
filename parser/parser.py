# -*- coding: utf-8 -*-
import re
import socket
import paramiko

mask = raw_input('enter log mask: ')
address = raw_input('enter ip: ')
ident = raw_input('enter ident: ')

def ip_adress(address):
    try:
        socket.inet_aton(address)
        print 'All,ok'
    except socket.error:
        print 'IP is wrong'
        return False

def auth_data(address):
    auth = {'login': 'login', 'password': 'password'}
    return auth

def find_log(mask, ls):
    regex = re.compile(".*(%s).*" % mask)
    new_ls = [m.group(0) for l in ls for m in [regex.search(l)] if m]
    logname = new_ls[0]
    return logname

def find_id(log_contents, ident):
    nlist = log_contents.split('\n')
    regex = re.compile(".*(%s).*" % ident)
    needed_list = [m.group(0) for l in nlist for m in [regex.search(l)] if m]
    needed_line = needed_list[0]
    index_nline = nlist.index(needed_line)
    final_list = nlist[index_nline - 100:index_nline + 100]
    for line in final_list:
        print line


ip_validator(address)

host = address
port = 22
login = auth_data()['login']
password = auth_data()['password']

client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname=host, username=login, password=password, port=port)

stdin, stdout, stderr = client.exec_command('ls')
spisok = stdout.read()
spisok_1 = spisok.split('\n')

logname = find_log(mask, spisok_1)

stdin, stdout, stderr = client.exec_command('cat %s' % log_name)
file_content= stdout.read()
client.close()


find_id(file_content, ident)